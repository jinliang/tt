package com.mogujie.ares.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.mogujie.ares.data.Department;
import com.mogujie.ares.data.User;
import com.mogujie.ares.lib.logger.Logger;
import com.mogujie.ares.lib.logger.LoggerFactory;
import com.mogujie.ares.manager.DBManager;
import com.mogujie.ares.manager.DBManager.DBPoolName;

/*
 * @Description: 部门相关的model
 * @author ziye
 * 
 */
public class DepartModel {

	private static DepartModel instance = new DepartModel();
	@SuppressWarnings("unused")
	private final Logger logger = LoggerFactory.getLogger(DepartModel.class);
	

	public static DepartModel getInstance() {
		if (instance == null) {
			instance = new DepartModel();
		}
		return instance;
	}

	/*
	 * @Description: 获取所有部门信息
	 * @return Map<Integer, Department> 部门的具体信息Map
	 * @throws SQLException
	 */
	public Map<Integer, Department> getDepartmentInfo(int fromUserId)
			throws SQLException {
		Map<Integer, Department> departInfos = new HashMap<Integer, Department>();
		DBManager dbManager = DBManager.getInstance();
		Connection conn = dbManager.getConnection(DBPoolName.macim_slave);
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			String sql = "select * from IMDepartment info where info.status = 0 and info.id=375";
			statement = conn.prepareStatement(sql);
			rs = statement.executeQuery();
			Department department = null;
			int departId = 0;
			while (rs.next()) {
				department = new Department();
				departId = rs.getInt("id");
				department.setDepartId(departId);
				department.setTitle(rs.getString("title"));
				department.setLeader(rs.getInt("leader"));
				//department.setParentDepartId(rs.getInt("pid"));
				department.setParentDepartId(0);
				department.setStatus(rs.getInt("status"));
				department.setDescription(rs.getString("descript") == null ? "0"
						: rs.getString("descript"));
				department.setGrade(rs.getInt("grade"));
				departInfos.put(departId, department);
			}
			logger.info("--------departmentSize:----------:"
					+ departInfos.size());
		} catch (SQLException e) {
			throw e;
		} finally {
			dbManager.release(DBPoolName.macim_slave, conn, statement, rs);
		}
		return departInfos;
	}
	
	/**
	 * 获取用户所在的部门
	 * @author jinliang
	 * @throws SQLException 
	 */
	public Department getDepartByUserId(Integer fromUserId) throws SQLException {
		DBManager dbManager = DBManager.getInstance();
		Connection conn = dbManager.getConnection(DBPoolName.macim_slave);
		PreparedStatement statement = null;
		ResultSet rs = null;
		Department department = null;
		try {
			String sql = "select * from IMDepartment info where info.id = (select  departId  from IMUsers tuser where tuser.id=?) ";
			statement = conn.prepareStatement(sql);
			statement.setInt(1, fromUserId);
			rs = statement.executeQuery();
			while (rs.next()) {
				department = new Department();
				department.setDepartId(rs.getInt("id"));
				department.setTitle(rs.getString("title"));
				department.setLeader(rs.getInt("leader"));
				department.setParentDepartId(rs.getInt("pid"));
				department.setStatus(rs.getInt("status"));
				department.setDescription(rs.getString("descript") == null ? "0"
						: rs.getString("descript"));
				department.setGrade(rs.getInt("grade"));
			}
		} catch (SQLException e) {
			throw e ;
		} finally {
			dbManager.release(DBPoolName.macim_slave, conn, statement, rs);
		}
		return department;
	}
	/**
	 * 获取用户所在部门的级别
	 * @author jinliang
	 * @throws SQLException 
	 */
	public int getUserDepartmentGrade(Integer userId) throws SQLException {
		Department  depart = getDepartByUserId(userId); 
		int grade = depart.getGrade();
		return grade;
	}
	
	/**
	 *  获取相同级别的部门集合
	 * @author jinliang
	 * @param grade
	 * @return
	 * @throws SQLException
	 */
	public 	Map<Integer, Department>  getDepartChildsByIds(String ids) throws SQLException{
		
		Map<Integer, Department> departInfos = new HashMap<Integer, Department>();
		DBManager dbManager = DBManager.getInstance();
		Connection conn = dbManager.getConnection(DBPoolName.macim_slave);
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			String sql = "select  * from  imdepartment  info where info.id in (?) ";
			statement = conn.prepareStatement(sql);
			statement.setString(1, ids);
			rs = statement.executeQuery();
			Department department = null;
			int departId = 0;
			while (rs.next()) {
				department = new Department();
				department.setDepartId(rs.getInt("id"));
				department.setTitle(rs.getString("title"));
				department.setLeader(rs.getInt("leader"));
				department.setParentDepartId(rs.getInt("pid"));
				department.setStatus(rs.getInt("status"));
				department.setDescription(rs.getString("descript") == null ? "0"
						: rs.getString("descript"));
				department.setGrade(rs.getInt("grade"));
				departInfos.put(departId, department);
			}
			logger.info("--------departmentSize:----------:"
					+ departInfos.size());
		} catch (SQLException e) {
			throw e;
		} finally {
			dbManager.release(DBPoolName.macim_slave, conn, statement, rs);
		}
		return departInfos;
	}
	
	/**
	 * 根据id 获取所有的子部门的信息
	 * @author jinliang
	 * @param departId
	 * @return
	 * @throws SQLException
	 */
	public  String getDepartChildIdsById(int departId) throws SQLException{
		DBManager dbManager = DBManager.getInstance();
		Connection conn = dbManager.getConnection(DBPoolName.macim_slave);
		PreparedStatement statement = null;
		ResultSet rs = null;
		String ids ="0" ;
		try {
			String sql = "select  getDepartChildList(?) ";
			statement = conn.prepareStatement(sql);
			statement.setInt(1, departId);
			rs = statement.executeQuery();
		    while (rs.next()){
		         ids = rs.getString(1);
		    	logger.info("title:" + ids);
		    }
		} catch (SQLException e) {
			throw e;
		} finally {
			dbManager.release(DBPoolName.macim_slave, conn, statement, rs);
		}
		return ids;
	}
	//-----------------2014-12-16 存储过程的书写 jinliang
	
	/**
	 * 获取自己部门的最高的根节点的部门
	 * @param departId
	 * @param grade
	 * @return
	 */
	public int getTopDepartRootByIdGrade(int departId, int grade){
		DBManager dbManager = DBManager.getInstance();
		Connection conn = dbManager.getConnection(DBPoolName.macim_slave);
		PreparedStatement statement = null;
		ResultSet rs = null;
		String getTopDepartSql = " select getParentGradeLevelByid(? , ? )  " ;
		int rootId =0 ;
		try {
			statement=conn.prepareStatement(getTopDepartSql);
			statement.setInt(1, departId);
			statement.setInt(2, grade);
			rs = statement.executeQuery();
			// 获取自己部门的根节点的信息
		    while (rs.next()){
		         rootId = rs.getInt(1);
		    }
		  
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rootId;
	}
	/**
	 * 根据根节点获取所有部门的孩子节点
	 * @author jinliang
	 * @param rootId
	 * @return
	 */
	public String  getChildDepartIdsByPId(int rootId){
		DBManager dbManager = DBManager.getInstance();
		Connection conn = dbManager.getConnection(DBPoolName.macim_slave);
		PreparedStatement statement = null;
		ResultSet rs = null;
		String getTopDepartSql = "  select  getDepartChildList(?)  " ;
		String ids =null;
		try {
			statement=conn.prepareStatement(getTopDepartSql);
			statement.setInt(1, rootId);
			rs = statement.executeQuery();
			// 获取自己部门的根节点的信息
		    while (rs.next()){
		         ids = rs.getString(1);
		    }
		  
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ids;
	}
	
	public 	Map<Integer, Department> getDepartsByIds(String ids){
		Map<Integer, Department> departInfos = new HashMap<Integer, Department>();
		DBManager dbManager = DBManager.getInstance();
		Connection conn = dbManager.getConnection(DBPoolName.macim_slave);
		PreparedStatement statement = null;
		ResultSet rs = null;
		
		// sql
	    StringBuffer sbBuffer = new StringBuffer() ;
        sbBuffer.append(" select * from imdepartment info where info.id in ( ") ;
        sbBuffer.append(ids);
        sbBuffer.append(" )  order by info.id desc ");
    	int departId = 0;
        try {
			statement=conn.prepareStatement(sbBuffer.toString());
			rs = statement.executeQuery();
			// 获取自己部门的根节点的信息
			Department department =null;
		    while (rs.next()){
		    	department = new Department();
		    	departId = rs.getInt("id");
				department.setDepartId(departId);
				department.setTitle(rs.getString("title"));
				department.setLeader(rs.getInt("leader"));
				department.setParentDepartId(rs.getInt("pid"));
				department.setStatus(rs.getInt("status"));
				department.setDescription(rs.getString("descript") == null ? "0"
						: rs.getString("descript"));
				department.setGrade(rs.getInt("grade"));
				departInfos.put(departId, department);
		    }
		  
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return departInfos;
	}

	public Map<Integer, Department> getDepartsLimit() {
		Map<Integer, Department> departInfos = new HashMap<Integer, Department>();
		DBManager dbManager = DBManager.getInstance();
		Connection conn = dbManager.getConnection(DBPoolName.macim_slave);
		PreparedStatement statement = null;
		ResultSet rs = null;
		
		// sql
	    StringBuffer sbBuffer = new StringBuffer() ;
        sbBuffer.append(" select * from imdepartment info   limit  5  ") ;
      
    	int departId = 0;
        try {
			statement=conn.prepareStatement(sbBuffer.toString());
			rs = statement.executeQuery();
			// 获取自己部门的根节点的信息
			Department department =null;
		    while (rs.next()){
		    	department = new Department();
		    	departId = rs.getInt("id");
				department.setDepartId(departId);
				department.setTitle(rs.getString("title"));
				department.setLeader(rs.getInt("leader"));
				department.setParentDepartId(rs.getInt("pid"));
				department.setStatus(rs.getInt("status"));
				department.setDescription(rs.getString("descript") == null ? "0"
						: rs.getString("descript"));
				department.setGrade(rs.getInt("grade"));
				departInfos.put(departId, department);
		    }
		  
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return departInfos;
		
	}
	
	
}
