package com.mogujie.ares.model;

public interface TTSqlConstant {
	
	
	// 文件模块
    String getMySendFileSql ="select * from IMTransmitFile where "
            + "  userId = ? and created > ? and status = 1 order by created desc limit 50"; //获取我发送的文件
   
    String getMyUnReceiveFileSql ="select * from IMTransmitFile where "
            + " toUserId = ? and created > ? and status = 1 order by created desc limit 50";// 获取我未接受的文件
    
    String getAlreadyReceiveFileSql ="select * from IMTransmitFile where "
            + " toUserId = ? and created > ? and status = 0 order by created desc limit 50"; ;//获取我接受的文件
}
